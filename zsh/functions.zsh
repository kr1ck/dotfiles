####################
# functions
####################

#reload config 

function rl() {
 source ~/.zshrc
}
# print available colors and their numbers
function colours() {
    for i in {0..255}; do
        printf "\x1b[38;5;${i}m colour${i}"
        if (( $i % 5 == 0 )); then
            printf "\n"
        else
            printf "\t"
        fi
    done
}

# Create a new directory and enter it
function md() {
    mkdir -p "$@" && cd "$@"
}

function hist() {
    history | awk '{a[$2]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head
}

#temp audio track switch
function temp_ff(){
 for file in *.m4v
 do
     ffmpeg -i "${file}" -map 0:3 -map 0:1 -map 0:2 -map 0:0 -map 0:4 -map 0:5 -map 0:6 -c:s mov_text -acodec copy -vcodec copy temp.m4v && rm "${file}" && mv temp.m4v "${file}"
 done
}

# find shorthand
function f() {
    find . -name "$1"
}

function ng-stop() {
    sudo launchctl stop homebrew.mxcl.nginx
}

function ng-start() {
    sudo launchctl start homebrew.mxcl.nginx
}
function ng-restart() {
     sudo launchctl start homebrew.mxcl.nginx
}


# Start an HTTP server from a directory, optionally specifying the port
function server() {
    local port=$[${RANDOM}%45535+20000]
    if [ "$1" != "" ] ; then
        port=$1
    fi
    open "http://localhost:${port}/"
    # Set the default Content-Type to `text/plain` instead of `application/octet-stream`
    # And serve everything as UTF-8 (although not technically correct, this doesn’t break anything for binary files)
    python -c $'import SimpleHTTPServer;\nmap = SimpleHTTPServer.SimpleHTTPRequestHandler.extensions_map;\nmap[""] = "text/plain";\nfor key, value in map.items():\n\tmap[key] = value + ";charset=UTF-8";\nSimpleHTTPServer.test();' "$port"
}

# take this repo and copy it to somewhere else minus the .git stuff.
function gitexport(){
    mkdir -p "$1"
    git archive master | tar -x -C "$1"
}

# get gzipped size
function gz() {
    echo "orig size    (bytes): "
    cat "$1" | wc -c
    echo "gzipped size (bytes): "
    gzip -c "$1" | wc -c
}

# All the dig info
function digga() {
    dig +nocmd "$1" any +multiline +noall +answer
}

# Escape UTF-8 characters into their 3-byte format
function escape() {
    printf "\\\x%s" $(printf "$@" | xxd -p -c1 -u)
    echo # newline
}

# Decode \x{ABCD}-style Unicode escape sequences
function unidecode() {
    perl -e "binmode(STDOUT, ':utf8'); print \"$@\""
    echo # newline
}

# Extract archives - use: extract <file>
# Credits to http://dotfiles.org/~pseup/.bashrc
function extract() {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2) tar xjf $1 ;;
            *.tar.gz) tar xzf $1 ;;
            *.bz2) bunzip2 $1 ;;
            *.rar) rar x $1 ;;
            *.gz) gunzip $1 ;;
            *.tar) tar xf $1 ;;
            *.tbz2) tar xjf $1 ;;
            *.tgz) tar xzf $1 ;;
            *.zip) unzip $1 ;;
            *.Z) uncompress $1 ;;
            *.7z) 7z x $1 ;;
            *) echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

function scpp() {
    scp "$1" nnisi@nisi.org:~/nisi.org/i;
    echo "http://nicknisi.com/i/$1" | pbcopy;
    echo "Copied to clipboard: http://nicknisi.com/i/$1"
}

# install a grunt plugin and save to devDependencies
function gi() {
    npm install --save-dev grunt-"$@"
}

# install a grunt-contrib plugin and save to devDependencies
function gci() {
    npm install --save-dev grunt-contrib-"$@"
}

# syntax highlight the contents of a file or the clipboard and place the result on the clipboard
function hl() {
    if [ -z "$3" ]; then
        src=$( pbpaste )
    else
        src=$( cat $3 )
    fi

    if [ -z "$2" ]; then
        style="moria"
    else
        style="$2"
    fi

    echo $src | highlight -O rtf --syntax $1 --font Inconsoloata --style $style --line-number --font-size 24 | pbcopy
}

# set the background color to light
function light() {
    export BACKGROUND="light" && reload!
}

function dark() {
    export BACKGROUND="dark" && reload!
}

# fast folder copy
function dcp() {
    if  [ ! -d "$1" ]; then
        echo "first argument must be directory"
        return
    fi
    if [ "$2" == "" ]; then
        echo specify target
        return
    fi
    if [ "$1" == "$2" ]; then
        echo "cant sync to self"
        return
    fi
    if [ -d "$2" ]; then
        echo "$2" already exists
        return
    fi
    local src=${1:a}
    local dst=${2:a}
    printf "\x1b[38;5;140m"
    echo "$src"
    echo "=========>"
    echo "$dst"
    echo ""
    printf "\x1b[38;5;160m"
    echo "performing sync (cp on steroids)"
    mkdir $dst
    rsync -aHE --progress $src $dst

}

function drm() {
    if  [ ! -d "$1" ]; then
        echo "first argument must be directory"
        return
    fi
    local src=${1:a}
    local tempfolder="${0:a:h}/dcp_temp"
    printf "\x1b[38;5;140m"

    echo "removing $src"
    echo "=========>"
    echo ""
    echo "$tempfolder"
    printf "\x1b[38;5;160m"
    mkdir "$tempfolder"
    echo "performing sync (rm on steroids)"
    rsync -a --delete --progress --no-owner --no-perms $tempfolder $src
    rm -R $src
    rm -R $tempfolder
}

function longLoops(){
    local loopcount=30
    local workingDir=$PWD
    local outputDir=$workingDir/long-loops
    mkdir -p $outputDir
    local txt_list="$workingDir/temp.txt"
    rm $txt_list 2> /dev/null 

    for file in $workingDir/*; do
        if ! [ -f $file ]; then
            continue
        fi
        touch $txt_list
        local basename=$(basename ${file//' '/'\ '})
        for q in {1..$loopcount}; do
            local row="file $basename"
            echo $row >> $txt_list
        done
        echo "Now processing file ./$file"
        local output="$outputDir/$(basename $file)"
        ffmpeg -f concat -safe 0 -i $txt_list -c copy $output -y 
        rm $txt_list 2> /dev/null 
    done
}

function em14() {
    if [ "$1" == "" ]; then
        echo specify size
        return
    fi

    local size=14
    #echo $res
    echo "scale=2 ; $1 / $size" | bc
}

