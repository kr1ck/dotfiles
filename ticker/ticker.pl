use strict;
use utf8;
use warnings;
use Encode;

my $count = 30;
my $step = 2;

my $s = `osascript ~/.dotfiles/applescripts/spotify.scpt`;
$s=~s/(^\s+|\s+$)//g;
Encode::_utf8_on ($s);
my $m = "❮".$s."❯   ";
Encode::_utf8_on ($m);
my $track = ($ENV{"HOME"}.'/.dotfiles/ticker/current.txt');
open(my $fh, '<', $track)
  or die "Could not open file '$track' $!";
my $row = <$fh>;
close($fh);

open(my $o, '<:encoding(UTF-8)', $ENV{"HOME"}.'/.dotfiles/ticker/offset.txt')
  or die "no access to offset";
my $offset = <$o>;
close $o;

if ($row eq `osascript ~/.dotfiles/applescripts/spotify.scpt`)
{}
else{
        $offset = 0;
	open(my $off, '>', $ENV{"HOME"}.'/.dotfiles/ticker/offset.txt');
	print($off "0");
	close $off;
	my $zz = `osascript ~/.dotfiles/applescripts/spotify.scpt > $ENV{"HOME"}/.dotfiles/ticker/current.txt`;
}



my $len = length($m);
my $str = "";
Encode::_utf8_on ($str);
if ($len - $offset >= $count){
	$str = substr($m, $offset, $count);
} else {
	$str = substr($m, $offset, $count).substr($m, 0, $count - ($len - $offset));
}

my $nextoffset = $offset+$step;
if ($nextoffset > $len){
	$nextoffset = $nextoffset - $len;
}
open(my $of, '>', $ENV{"HOME"}.'/.dotfiles/ticker/offset.txt');
print($of $nextoffset);
close $of;
if($len-3 <= $count){
	$str = substr($m, 0, $len-3);
}

Encode::_utf8_on ($str);
chomp($str);
print "🔉  ".$str."\n";
# print "🎵  ".$str."\n";
