#### COLOUR

#tm_icon="♟"
tm_icon="🆔 "
tm_color_active=colour213
tm_color_inactive=colour241
tm_color_feature=colour4
tm_color_music=colour203
tm_color_username=colour196
tm_color_username_bg=colour52

# separators
tm_separator_left_bold="◀"
tm_separator_left_thin="❮"
tm_separator_right_bold="▶"
tm_separator_right_thin="❯"


set -g status-left-length 32
set -g status-right-length 150
set -g status-interval 1


# default statusbar colors
# set-option -g status-bg colour0
set-option -g status-style fg=$tm_color_active,bg=default,default

# default window title colors
set-window-option -g window-status-style fg=$tm_color_inactive,bg=default
set -g window-status-format "#I #W"

# active window title colors
set-window-option -g window-status-current-style fg=$tm_color_active,bg=default
set-window-option -g  window-status-current-format "#[bold]#I #W"

# pane border
set-option -g pane-border-style fg=$tm_color_inactive
set-option -g pane-active-border-style fg=$tm_color_active

# message text
set-option -g message-style bg=default,fg=$tm_color_active

# pane number display
set-option -g display-panes-active-colour $tm_color_active
set-option -g display-panes-colour $tm_color_inactive

# clock
set-window-option -g clock-mode-colour $tm_color_active

#tm_spotify="#[fg=$tm_color_music]#(osascript ~/.dotfiles/applescripts/spotify.scpt)"
tm_spotify="#[fg=$tm_color_music]#(perl -CS ~/.dotfiles/ticker/ticker.pl)"
tm_itunes="#[fg=$tm_color_music]#(osascript ~/.dotfiles/applescripts/itunes.scpt)"
tm_rdio="#[fg=$tm_color_music]#(osascript ~/.dotfiles/applescripts/rdio.scpt)"
tm_battery="#(~/.dotfiles/bin/battery_indicator.sh)"
tm_username="#[fg=$tm_color_username, bg=$tm_color_username_bg, bold] #(who | cut -d \" \" -f1)"

tm_date="#[fg=$tm_color_inactive] %R %b %d"
tm_host="#[fg=$tm_color_feature,bold]#h"
tm_session_name="#[fg=$tm_color_feature,bg=default,bold] $tm_icon #S "

set -g status-left $tm_username' '$tm_session_name
set -g status-right $tm_itunes' '$tm_rdio' '$tm_spotify' '$tm_date' '$tm_host

